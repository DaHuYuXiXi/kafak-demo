package com.kafkaDemo.raw.demo1;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;

import static raw.common.CommonConstants.KAFKA_ADDRESS;

public class KafkaConsumerTest {
    private static final String TEST_TOPIC = "test1";
    private Properties props;

    @BeforeEach
    public void prepareTest() {
        props = new Properties();
        //kafka集群信息
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_ADDRESS);
        //消费者组名称
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "dhy_group");
        //key的反序列化器
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        //value的反序列化器
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    }


    @Test
    public void consumeWithAutoCommit() {
        //下面两个属性用于设置自动提交---只要消息被拉取到,并且到了指定的间隔时间,消息便会自动提交
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.setProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        consumeTemplate(KafkaConsumerTest::printRecord, null);
    }

    @Test
    public void consumeWithNoAutoCommitWithSyncBatch() {
        //设置为手动提交--默认值
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //缓冲记录,用于批量手动提交
        List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
        final int minBatchSize = 3;
        consumeTemplate(buffer::add, consumer -> {
            if (buffer.size() >= minBatchSize) {
                //数据操作
                buffer.forEach(KafkaConsumerTest::printRecord);
                //批量提交
                consumer.commitSync();
                //清空缓存
                buffer.clear();
            }
        });
    }

    @Test
    public void consumeWithNoAutoCommitWithAsyncBatch() {
        //设置为手动提交--默认值
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //缓冲记录,用于批量手动提交
        List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
        final int minBatchSize = 3;
        consumeTemplate(buffer::add, consumer -> {
            if (buffer.size() >= minBatchSize) {
                //数据操作
                buffer.forEach(KafkaConsumerTest::printRecord);
                //批量提交
                consumer.commitAsync(new OffsetCommitCallback() {
                    @Override
                    public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception) {
                        if (exception != null) {
                            //异常处理
                        }
                    }
                });
                //清空缓存
                buffer.clear();
            }
        });
    }

    @Test
    public void consumeWithNoAutoCommitCombineSyncBatchAndAsyncBatch() {
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        consumer.subscribe(Collections.singletonList(TEST_TOPIC));
        try {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(100));
                records.forEach(KafkaConsumerTest::printRecord);
                consumer.commitAsync();
            }
        } catch (CommitFailedException e) {
            //记录错误日日志,进行告警处理
            e.printStackTrace();
        } finally {
            consumer.commitSync();
            consumer.close();
        }
    }

    @Test
    public void consumeWithNoAutoCommitWithSingle() {
        //设置为手动提交--默认值
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TEST_TOPIC));
        Map<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
        while (true) {
            //一次性请求尽可能多的数据
            ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
            for (ConsumerRecord<String, String> record : records) {
                printRecord(record);
                //记录下offsets信息
                offsets.put(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset() + 1));
                //当前消息处理完,就提交当前消息的偏移量
                consumer.commitAsync(offsets, null);
            }
            try {
                //处理完当前批次的消息,在拉取下一批消息前,调用commitSync方法提交当前批次最新消息
                consumer.commitSync(offsets);
            } catch (CommitFailedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * recordConsumer针对单条数据进行处理，此方法中应该做好异常处理，避免外围的while循环因为异常中断。
     */
    public void consumeTemplate(Consumer<ConsumerRecord<String, String>> recordConsumer, Consumer<KafkaConsumer<String, String>> afterCurrentBatchHandle) {
        //1.创建消费者
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        //2.订阅Topic--支持正则表达式: consumer.subscribe("test.*");
        consumer.subscribe(Collections.singletonList(TEST_TOPIC));
        try {
            while (true) {
                //循环拉取数据,
                //  Duration超时时间，如果有数据可消费，立即返回数据
                // 如果没有数据可消费，超过Duration超时时间也会返回，但是返回结果数据量为0
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(100));
                for (ConsumerRecord<String, String> record : records) {
                    recordConsumer.accept(record);
                }
                if (afterCurrentBatchHandle != null) {
                    afterCurrentBatchHandle.accept(consumer);
                }
            }
        } finally {
            //退出应用程序前使用close方法关闭消费者，
            // 网络连接和socket也会随之关闭，并立即触发一次再均衡
            consumer.close();
        }
    }

    private static void printRecord(ConsumerRecord<String, String> record) {
        System.out.println("topic:" + record.topic()
                + ",partition:" + record.partition()
                + ",offset:" + record.offset()
                + ",key:" + record.key()
                + ",value" + record.value());
        record.headers().forEach(System.out::println);
    }
}
