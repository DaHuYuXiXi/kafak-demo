package com.kafkaDemo.raw.demo1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;

import static raw.common.CommonConstants.KAFKA_ADDRESS;

/**
 * 生产者使用demo
 */
public class KafkaProducerTest {
    private static final String TEST_TOPIC = "test1";
    private Properties props;

    @BeforeEach
    public void prepareTest() {
        props = new Properties();
        //kafka broker列表
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_ADDRESS);
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 5000);
        //可靠性确认应答参数
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        //发送失败,重新尝试的次数
        props.put(ProducerConfig.RETRIES_CONFIG, "3");
        //生产者数据key序列化方式
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //生产者数据value序列化方式
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    }

    @Test
    public void sendAsyncWithNoCallBack() throws ExecutionException, InterruptedException {
        //ProducerRecord: 主题名,key,val
        sendMsgTemplate(data -> new ProducerRecord<>(TEST_TOPIC, data, "val: " + data), null);
    }

    @Test
    public void sendAsyncWithCallBack() throws ExecutionException, InterruptedException {
        //ProducerRecord: 主题名,key,val
        sendMsgTemplate(data -> new ProducerRecord<>(TEST_TOPIC, data, "val: " + data), ((recordMetadata, e) -> {
            if (e == null) {
                System.out.println("消息发送成功,消息所在分区为:" + recordMetadata.partition() + " ,在分区中的位移为:" + recordMetadata.offset());
            } else {
                e.printStackTrace();
            }
        }));
    }

    @Test
    public void sendSync() throws ExecutionException, InterruptedException {
        try (KafkaProducer<String, String> producer = new KafkaProducer<>(props)) {
            Future<RecordMetadata> future = producer.send(new ProducerRecord<>(TEST_TOPIC, Integer.toString(520), "val=520"));
            RecordMetadata recordMetadata = future.get();
            if (recordMetadata != null && recordMetadata.hasOffset()) {
                System.out.println("消息同步发送成功: " + recordMetadata.offset());
            }
        }
    }

    @Test
    public void sendBatch() throws InterruptedException, ExecutionException {
        //满足下面两个条件其中之一就会把消息缓冲区中的消息进行批量发送
        //每一批消息的大小: 默认是16KB
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        //延迟时间
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1000);
        sendMsgTemplate(data -> {
            try {
                //模拟业务请求耗时,使得能够满足上面延迟时间条件,触发批量发送
                Thread.sleep(1000);
                return new ProducerRecord<>(TEST_TOPIC, data, "val: " + data);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, null);
    }


    private void sendMsgTemplate(Function<String, ProducerRecord<String, String>> recordCallBack, Callback callback) throws ExecutionException, InterruptedException {
        //try-with-resource -- 自动释放资源 -- 因为KafkaProducer实现了AutoCloseable接口
        try (KafkaProducer<String, String> producer = new KafkaProducer<>(props)) {
            for (int i = 0; i < 20; i++) {
                producer.send(recordCallBack.apply(Integer.toString(i)), callback);
            }
        }
    }

}
