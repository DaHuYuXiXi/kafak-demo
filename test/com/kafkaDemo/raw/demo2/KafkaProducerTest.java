package com.kafkaDemo.raw.demo2;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import raw.pojo.Peo;
import raw.serializer.JacksonSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

/**
 * 生产者使用demo
 */
public class KafkaProducerTest {
    private static final String TEST_TOPIC = "test2";
    private Properties props;

    @BeforeEach
    public void prepareTest() {
        props = new Properties();
        //kafka broker列表
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 5000);
        //可靠性确认应答参数
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        //发送失败,重新尝试的次数
        props.put(ProducerConfig.RETRIES_CONFIG, "3");
        //生产者数据key序列化方式
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //序列化器的配置
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JacksonSerializer.class.getName());
        //拦截器的配置
        //props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, RequestStatCalInterceptor.class.getName());
        //分区器配置
       // props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, ValuePartitioner.class.getName());
    }

    @Test
    void sendAsyncWithNoCallBack() throws ExecutionException, InterruptedException {
        sendMsgTemplate(data -> new ProducerRecord<>(TEST_TOPIC, data, "val: " + data), null);
    }

    @Test
    void testJacksonSerializer() throws ExecutionException, InterruptedException {
        Peo peo = new Peo();
        peo.setName("dhy");
        peo.setAge(18);
        KafkaProducer<String, Peo> producer = new KafkaProducer<>(props);
        RecordMetadata metadata = producer.send(new ProducerRecord<>(TEST_TOPIC, peo)).get();
        System.out.println("消息偏移量为:"+metadata.offset());
    }

    private void sendMsgTemplate(Function<String, ProducerRecord<String, String>> recordCallBack, Callback callback) throws ExecutionException, InterruptedException {
        //try-with-resource -- 自动释放资源 -- 因为KafkaProducer实现了AutoCloseable接口
        try (KafkaProducer<String, String> producer = new KafkaProducer<>(props)) {
            for (int i = 0; i < 20; i++) {
                producer.send(recordCallBack.apply(Integer.toString(i)), callback);
            }
        }
    }

}
