package com.kafkaDemo.raw.demo2;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import raw.pojo.Peo;
import raw.serializer.JacksonDeserializer;

import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;

public class KafkaConsumerTest {
    private static final String TEST_TOPIC = "test2";
    private Properties props;

    @BeforeEach
    public void prepareTest() {
        props = new Properties();
        //kafka集群信息
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //消费者组名称
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "dhy_group");
        //key的反序列化器
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        //value的反序列化器
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JacksonDeserializer.class.getName());
    }


    @Test
    void consumeWithAutoCommit() {
        //下面两个属性用于设置自动提交---只要消息被拉取到,并且到了指定的间隔时间,消息便会自动提交
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.setProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        consumeTemplate(KafkaConsumerTest::printRecord, null);
    }

    @Test
    void testDeserializer(){
        //1.创建消费者
        KafkaConsumer<String, Peo> consumer = new KafkaConsumer<>(props);
        //2.订阅Topic
        consumer.subscribe(Collections.singletonList(TEST_TOPIC));

        try {
            while (true) {
                //循环拉取数据,
                //Duration超时时间，如果有数据可消费，立即返回数据
                // 如果没有数据可消费，超过Duration超时时间也会返回，但是返回结果数据量为0
                ConsumerRecords<String, Peo> records = consumer.poll(Duration.ofSeconds(100));
                for (ConsumerRecord<String, Peo> record : records) {
                    System.out.println(record.value());
                }
            }
        } finally {
            //退出应用程序前使用close方法关闭消费者，
            // 网络连接和socket也会随之关闭，并立即触发一次再均衡（再均衡概念后续章节介绍）
            consumer.close();
        }
    }

    /**
     * recordConsumer针对单条数据进行处理，此方法中应该做好异常处理，避免外围的while循环因为异常中断。
     */
    public void consumeTemplate(Consumer<ConsumerRecord<String, String>> recordConsumer, Consumer<KafkaConsumer<String, String>> afterCurrentBatchHandle) {
        //1.创建消费者
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        //2.订阅Topic--支持正则表达式: consumer.subscribe("test.*");
        consumer.subscribe(Collections.singletonList(TEST_TOPIC));
        try {
            while (true) {
                //循环拉取数据,
                //  Duration超时时间，如果有数据可消费，立即返回数据
                // 如果没有数据可消费，超过Duration超时时间也会返回，但是返回结果数据量为0
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(100));
                for (ConsumerRecord<String, String> record : records) {
                    recordConsumer.accept(record);
                }
                if (afterCurrentBatchHandle != null) {
                    afterCurrentBatchHandle.accept(consumer);
                }
            }
        } finally {
            //退出应用程序前使用close方法关闭消费者，
            // 网络连接和socket也会随之关闭，并立即触发一次再均衡
            consumer.close();
        }
    }

    private static void printRecord(ConsumerRecord<String, String> record) {
        System.out.println("topic:" + record.topic()
                + ",partition:" + record.partition()
                + ",offset:" + record.offset()
                + ",key:" + record.key()
                + ",value" + record.value());
        record.headers().forEach(System.out::println);
    }
}
