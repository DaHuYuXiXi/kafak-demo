package com.kafkaDemo.raw.demo4;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ConsumerGroupThreadPoolTest {
    @Test
    void test(){
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 3; i++) {
            executorService.execute(new MyConsumer());
        }
    }
}
