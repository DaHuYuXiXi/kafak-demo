package com.kafkaDemo.raw.demo3;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * 生产者使用demo
 */
public class KafkaTransactionProducerTest {
    private static final String TEST_TOPIC = "test1";
    private Properties props;

    @BeforeEach
    public void prepareTest() {
        props = new Properties();
        //kafka broker列表
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 5000);
        //可靠性确认应答参数
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        //发送失败,重新尝试的次数
        props.put(ProducerConfig.RETRIES_CONFIG, "3");
        //生产者数据key序列化方式
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //序列化器的配置
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //生产者端开启幂等
        props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, Boolean.TRUE);
        //生产者端开启事务
        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "test-transaction");
    }


    @Test
    void testTransaction() throws ExecutionException, InterruptedException {
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        //0.初始化事务管理器
        producer.initTransactions();
        //1.开启事务
        producer.beginTransaction();
        try {
            //2.发送消息
            producer.send(new ProducerRecord<>(TEST_TOPIC, Integer.toString(1), "test1"));
            producer.send(new ProducerRecord<>(TEST_TOPIC, Integer.toString(2), "test2"));
            producer.send(new ProducerRecord<>(TEST_TOPIC, Integer.toString(3), "test3"));
            //3.提交事务
            producer.commitTransaction();
        } catch (ProducerFencedException e) {
            e.printStackTrace();
            //4.1 事务回滚
            producer.abortTransaction();
        } catch (KafkaException e) {
            e.printStackTrace();
            //4.2 事务回滚
            producer.abortTransaction();
        } finally {
            producer.close();
        }
    }
}
