package com.kafkaDemo.springboot;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFutureCallback;
import springboot.KafkaSpringBootDemo;
import springboot.pojo.User;

import javax.annotation.Resource;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static springboot.kafka.KafkaConstants.TEST_KAFKA_TOPIC;

@SpringBootTest(classes = KafkaSpringBootDemo.class)
@Slf4j
class SpringKafkaTest {

    @Resource
    KafkaTemplate<String, User> kafkaTemplate;

    @Test
    void testProducer() throws IOException {
        User user = new User();
        user.setAge(21);
        user.setName("大忽悠");
        kafkaTemplate.send(TEST_KAFKA_TOPIC, user);
        System.in.read();
    }

    @Test
    void testAsyncWithCallBack() throws IOException {
        User user = new User();
        user.setAge(21);
        user.setName("大忽悠");
        kafkaTemplate.send(TEST_KAFKA_TOPIC, user).addCallback(new ListenableFutureCallback<SendResult<String, User>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("msg send err: ", ex);
            }

            @Override
            public void onSuccess(SendResult<String, User> result) {
                // 消息发送到的topic
                String topic = result.getRecordMetadata().topic();
                // 消息发送到的分区
                int partition = result.getRecordMetadata().partition();
                // 消息在分区内的offset
                long offset = result.getRecordMetadata().offset();
                log.info("msg send success,topic: {},partition: {},offset: {}", topic, partition, offset);
            }
        });
        System.in.read();
    }

    @Test
    void testSync() throws IOException {
        User user = new User();
        user.setAge(21);
        user.setName("大忽悠");
        try {
            SendResult<String, User> result = kafkaTemplate.send(TEST_KAFKA_TOPIC, user).get();
            // 消息发送到的topic
            String topic = result.getRecordMetadata().topic();
            // 消息发送到的分区
            int partition = result.getRecordMetadata().partition();
            // 消息在分区内的offset
            long offset = result.getRecordMetadata().offset();
            log.info("msg send success,topic: {},partition: {},offset: {}", topic, partition, offset);
        } catch (InterruptedException | ExecutionException e) {
            log.error("send msg sync occurs err: ", e);
        }
        System.in.read();
    }

    @Test
    void testTransaction() {
        User user = new User();
        user.setAge(21);
        user.setName("大忽悠");
        //调用事务模板方法
        kafkaTemplate.executeInTransaction(operations -> {
            operations.send(TEST_KAFKA_TOPIC, user);
            //业务处理发生异常,事务回滚
            throw new RuntimeException("fail");
        });
    }
}