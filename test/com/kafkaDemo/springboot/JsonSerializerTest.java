package com.kafkaDemo.springboot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import springboot.KafkaSpringBootDemo;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest(classes = KafkaSpringBootDemo.class)
class JsonSerializerTest {
    @Resource
    private ObjectMapper objectMapper;

    private ObjectMapper objectMapperNew=new ObjectMapper();

    @Test
    void dateSerializerTest() throws JsonProcessingException {
        System.out.println("spring注入的ObjectMapper序列化结果: "+objectMapper.writeValueAsString(new Date()));
        System.out.println("手动new的ObjectMapper序列化结果: "+objectMapperNew.writeValueAsString(new Date()));
    }

    @Test
    void configureTest() throws JsonProcessingException {
        objectMapperNew.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapperNew.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        System.out.println("手动new的ObjectMapper序列化结果: "+objectMapperNew.writeValueAsString(new Date()));
    }
}
