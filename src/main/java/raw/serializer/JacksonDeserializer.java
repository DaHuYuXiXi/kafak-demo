package raw.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import raw.pojo.Peo;

import java.io.IOException;

/**
 * 反序列化器
 */
public class JacksonDeserializer implements Deserializer<Peo> {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public Peo deserialize(String topic, byte[] data) {
        try {
            return objectMapper.readValue(data,Peo.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
