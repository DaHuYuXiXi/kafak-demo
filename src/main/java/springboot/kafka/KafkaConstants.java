package springboot.kafka;

public interface KafkaConstants {
    String TEST_KAFKA_TOPIC="test3";
    String TEST_CONSUMER_GROUP="test_group";
}
