package springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaSpringBootDemo {
    public static void main(String[] args) {
        SpringApplication.run(KafkaSpringBootDemo.class,args);
    }
}
