package springboot.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;
import springboot.pojo.User;

import java.util.List;

import static springboot.kafka.KafkaConstants.TEST_CONSUMER_GROUP;
import static springboot.kafka.KafkaConstants.TEST_KAFKA_TOPIC;

@Component
@Slf4j
public class KafkaConsumer {
    @KafkaListener(topics = TEST_KAFKA_TOPIC, groupId = TEST_CONSUMER_GROUP)
    public void dealUser(List<User> user,Acknowledgment ack) {
        user.forEach(System.out::println);
        ack.acknowledge();
    }
}